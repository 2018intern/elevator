/*
*/

#ifndef ELEVATOR_H
#define ELEVATOR_H

#include "Arduino.h"

#define TOTAL_FLOORS				4
#define TOTAL_ELEVATORS				2
#define DIRECTIONS					2
#define DIR_UP						0
#define DIR_DOWN					1
#define DIR_NONE					2
#define BUTTON_PRESSED				1
#define BUTTON_NOT_PRESSED			0
#define	ERROR						1
#define OK							0

// Button Defines for Nano
// Inside Left Elevator
#define LE0							A0
#define LE1							A2
#define LE2							A1
#define LE3							A3
// Inside Right ElevatorA3
#define RE0							A5
#define RE1							12
#define RE2							A4
#define RE3							10
// Outside Floor Buttons
#define ZEROUP						4
#define ZERODOWN					4
#define ONEUP						2
#define ONEDOWN						3
#define TWOUP						6
#define TWODOWN						5
#define THREEUP						7
#define THREEEDOWN					7

#define FLOOR2FLOOR_TIME			2000

// #define TOTAL_BUTTONS				14

class elevatorClass {
	public:
	elevatorClass();
	// Button Array inside the elevators
	uint8_t elevator1ButtonArray[TOTAL_FLOORS]	= {BUTTON_NOT_PRESSED}; 
	uint8_t elevator2ButtonArray[TOTAL_FLOORS]	= {BUTTON_NOT_PRESSED};

	// Elevator Direction	
	uint8_t elevator1Direction					= DIR_NONE;
	uint8_t elevator2Direction					= DIR_NONE;	

	// Current Floor the elevatorClass is on
	uint8_t elevator1CurrentFloor				= 0; 
	uint8_t elevator2CurrentFloor				= 0;

	// Floor Buttons
	uint8_t floorButton[TOTAL_FLOORS][DIRECTIONS]	= {{BUTTON_NOT_PRESSED}};

	// Function to move the elevatorClass to a particular floor
	void moveElevator1(uint8_t toFloor);
	void moveElevator2(uint8_t toFloor);

	void execute(void);

	private:
	
	// uint8_t buttonList [TOTAL_BUTTONS];
	uint8_t elevatorButtonIndex[TOTAL_ELEVATORS][TOTAL_FLOORS]				= {{LE0, LE1, LE2, LE3}, {RE0, RE1, RE2, RE3}}; 
	uint8_t floorButtonIndex[TOTAL_FLOORS][DIRECTIONS]						= {{ZEROUP, ZERODOWN}, {ONEUP, ONEDOWN}, {TWOUP, TWODOWN}, {THREEUP, THREEEDOWN}};


	// Timers
	unsigned long elevator1Timer				= 0;
	unsigned long elevator2Timer				= 0;
	// unsigned long elevator1CurrentFloor;
	// unsigned long elevator2CurrentFloor;
	uint8_t elevator1Destination				= 0;
	uint8_t elevator2Destination				= 0;

	void check_buttons(void);

};

extern elevatorClass elevator;  // elevator object 

#endif	// ELEVATOR_H
