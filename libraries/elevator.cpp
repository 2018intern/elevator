/*
  03/04/2018
  Elevator Class for use with the Arduino Code
*/

#include "elevator.h"

elevatorClass::elevatorClass()
{
	// Initialize the system

	// Set all pins as inputs
	pinMode(LE0, INPUT);
	pinMode(LE1, INPUT);
	pinMode(LE2, INPUT);
	pinMode(LE3, INPUT);
	pinMode(RE0, INPUT);
	pinMode(RE1, INPUT);
	pinMode(RE2, INPUT);
	pinMode(RE3, INPUT);
	pinMode(ZEROUP, INPUT);
	pinMode(ONEUP, INPUT);
	pinMode(ONEDOWN, INPUT);
	pinMode(TWOUP, INPUT);
	pinMode(TWODOWN, INPUT);
	pinMode(THREEEDOWN, INPUT);

	// Initialize internal status stuff
	// Initialize the list of the buttons
	// Variables used
}

/*
void elevatorClass::init(void)
{
	Serial.println("Elevator Initialized");

}
*/

void elevatorClass::moveElevator1(uint8_t toFloor)
{
	// Moving Elevator 1 to destination
	if(elevator1Direction != DIR_NONE)			// Moving
	{
		Serial.println("Trig 1 while moving");
		if(elevator1Direction == DIR_UP)		// Moving up
		{
			// Check if the floor is in the direction
			if(elevator1CurrentFloor <= toFloor)
			{
				elevator1Destination = toFloor;
				elevator1Timer = millis();
			}
			else		Serial.println("Error: Invalid floor as per up direction");
		}
		else									// Moving down
		{
			if(elevator1CurrentFloor >= toFloor)
			{
				elevator1Destination = toFloor;
				elevator1Timer = millis();
			}
			else		Serial.println("Error: Invalid floor as per down direction");
		}
	}
	else										// Stationary
	{
		Serial.println("Trig 1 while not moving");
		elevator1Destination = toFloor;
		elevator1Timer = millis();
		if(elevator1CurrentFloor < toFloor)		// We are below the destination
		{
			Serial.print("E1 Moving to Floor ");
			elevator1Direction = DIR_UP;
			elevator1CurrentFloor ++;
		}
		else if(elevator1CurrentFloor > toFloor) // We are above the destination
		{
			Serial.print("E1 Moving to Floor ");
			elevator1Direction = DIR_DOWN;
			elevator1CurrentFloor --;
		}
		else				// We are already on the floor
		{
			Serial.print("Error: Opening doors on Floor ");
		}
		Serial.println(elevator1CurrentFloor);
	}
}

void elevatorClass::moveElevator2(uint8_t toFloor)
{
	// Moving Elevator 2 to destination
	if(elevator2Direction != DIR_NONE)			// Moving
	{
		Serial.println("Trig 2 while moving");
		if(elevator2Direction == DIR_UP)		// Moving up
		{
			// Check if the floor is in the direction
			if(elevator2CurrentFloor <= toFloor)
			{
				elevator2Destination = toFloor;
				elevator2Timer = millis();
			}
			else		Serial.println("Error: Invalid floor as per up direction");
		}
		else									// Moving down
		{
			if(elevator2CurrentFloor >= toFloor)
			{
				elevator2Destination = toFloor;
				elevator2Timer = millis();
			}
			else		Serial.println("Error: Invalid floor as per down direction");
		}
	}
	else										// Stationary
	{
		Serial.println("Trig 2 while not moving");
		elevator2Destination = toFloor;
		elevator2Timer = millis();
		if(elevator2CurrentFloor < toFloor)		// We are below the destination
		{
			Serial.print("E2 Moving to Floor ");
			elevator2Direction = DIR_UP;
			elevator2CurrentFloor ++;
		}
		else if(elevator2CurrentFloor > toFloor) // We are above the destination
		{
			Serial.print("E2 Moving to Floor ");
			elevator2Direction = DIR_DOWN;
			elevator2CurrentFloor --;
		}
		else				// We are already on the floor
		{
			Serial.print("Error: Opening doors on Floor ");
		}
		Serial.println(elevator2CurrentFloor);
	}
}

void elevatorClass::execute()
{
	// Function to check if a particular button is pressed or not
	check_buttons();

	// Floor Timer check function
	if(elevator1Direction != DIR_NONE)						// Moving
	{
		// Elevator 1
		if(millis() > elevator1Timer + FLOOR2FLOOR_TIME)
		{
			if(elevator1CurrentFloor == elevator1Destination)			// Stop here
			{
				// Stop Moving
				elevator1ButtonArray[elevator1CurrentFloor] = BUTTON_NOT_PRESSED;
				Serial.print("E1 Reached destination Floor ");
				Serial.println(elevator1CurrentFloor);
				// Clear the floor button
				floorButton[elevator1CurrentFloor][elevator1Direction] = BUTTON_NOT_PRESSED;
				elevator1Direction = DIR_NONE;
			}
			else
			{
				if(elevator1Direction == DIR_UP)			elevator1CurrentFloor++;
				else										elevator1CurrentFloor--;
				Serial.print("Moving Elevator 1 to next floor ");
				Serial.println(elevator1CurrentFloor);
				elevator1Timer = millis();
			}
		}
	}
		// Elevator 2
	if(elevator2Direction != DIR_NONE)						// Moving
	{
		if(millis() > elevator2Timer + FLOOR2FLOOR_TIME)
		{
			if(elevator2CurrentFloor == elevator2Destination)			// Stop here
			{
				// Stop Moving
				elevator2ButtonArray[elevator2CurrentFloor] = BUTTON_NOT_PRESSED;
				Serial.print("E2 Reached destination Floor ");
				Serial.println(elevator2CurrentFloor);
				// Clear the floor button
				floorButton[elevator1CurrentFloor][elevator1Direction] = BUTTON_NOT_PRESSED;
				elevator2Direction = DIR_NONE;
			}
			else
			{
				if(elevator2Direction == DIR_UP)			elevator2CurrentFloor++;
				else										elevator2CurrentFloor--;
				Serial.print("Moving Elevator 2 to next floor ");
				Serial.println(elevator2CurrentFloor);
				elevator2Timer = millis();
			}
		}
	}
}

void elevatorClass::check_buttons(void)
{
	// Button setting function
	// for(uint8_t bl = 0; bl < TOTAL_BUTTONS; bl++)
	// Check internal buttons of each elevator
	for(uint8_t floorNo = 0; floorNo < TOTAL_FLOORS; floorNo++)
	{
		// Check Elevator 1 buttons
		if (elevator1ButtonArray[floorNo] == BUTTON_NOT_PRESSED)
		{
			// Reset all Buttons
			digitalWrite(elevatorButtonIndex[0][floorNo], LOW);
			pinMode(elevatorButtonIndex[0][floorNo], INPUT);
			if(digitalRead(elevatorButtonIndex[0][floorNo]) == HIGH)		// Button is pressed
			{
				// Change the direction and
				pinMode(elevatorButtonIndex[0][floorNo], OUTPUT);
				// Set to HIGH
				digitalWrite(elevatorButtonIndex[0][floorNo], HIGH);
				// Set the bit in the array
				elevator1ButtonArray[floorNo] = BUTTON_PRESSED;
				Serial.print("Pressed Floor ");
				Serial.print(floorNo);
				Serial.println(" for Elevator 1");
			}
		}
		// Repeat for second elevator
		if (elevator2ButtonArray[floorNo] == BUTTON_NOT_PRESSED)
		{
			digitalWrite(elevatorButtonIndex[1][floorNo], LOW);
			pinMode(elevatorButtonIndex[1][floorNo], INPUT);
			if(digitalRead(elevatorButtonIndex[1][floorNo]) == HIGH)		// Button is pressed
			{
				// Change the direction and
				pinMode(elevatorButtonIndex[1][floorNo], OUTPUT);
				// Set to HIGH
				digitalWrite(elevatorButtonIndex[1][floorNo], HIGH);
				// Set the bit in the array
				elevator2ButtonArray[floorNo] = BUTTON_PRESSED;
				Serial.print("Pressed Floor ");
				Serial.print(floorNo);
				Serial.println(" for Elevator 2");
			}
		}

		// Check for outside buttons
		for (uint8_t direction = 0; direction < 2; direction++)			// There are only two directions
		{
			if(floorButton[floorNo][direction] == BUTTON_NOT_PRESSED)
			{
				// Check for valid buttons only
				if((floorNo == 0 && direction == DIR_DOWN) || 
				   (floorNo == 3 && direction == DIR_UP))
				{
					// Serial.println("Invalid Button");
				}
				else
				{
					digitalWrite(floorButtonIndex[floorNo][direction], LOW);
					pinMode(floorButtonIndex[floorNo][direction], INPUT);
					if(digitalRead(floorButtonIndex[floorNo][direction]) == HIGH)	// Button is pressed
					{
						// Change the direction and
						pinMode(floorButtonIndex[floorNo][direction], OUTPUT);
						// Set to HIGH
						digitalWrite(floorButtonIndex[floorNo][direction], HIGH);
						// Set the bit in the array
						floorButton[floorNo][direction] = BUTTON_PRESSED;
						if (direction == DIR_UP)				Serial.print("Up ");
						else if (direction == DIR_DOWN)			Serial.print("Down ");
						else									Serial.print("Error ");
						Serial.print("Button Pressed on Floor ");
						Serial.println(floorNo);
					}
				}
			}
		}
	}
}

/*
void executeElevator1()
{
	// Elevator 1
	if(elevator1Direction != DIR_NONE)			// Elevator is moving
	{
		if ((elevator1Timer + FLOOR2FLOOR_TIME) < millis())		// Reached New Floor
		{
			if (elevator1CurrentFloor == elevator1Destination)			// Reached the floor
			{
				Serial.print("Elevator 1 Reached Destination floor ");
				Serial.println(elevator1CurrentFloor);
				// Clear the Floor Latched
				// Check if the direction is up or down and clear respective outside latch
				if (elevator1Direction == DIR_UP)				// Up direction
				{
					floorButton[elevator1CurrentFloor][DIR_UP] = BUTTON_NOT_PRESSED;
					Serial.print("Cleared Floor ");
					Serial.print(elevator1CurrentFloor);
					Serial.println(" Up Button");
				}
				else											// Down direction
				{
					floorButton[elevator1CurrentFloor][DIR_DOWN] = BUTTON_NOT_PRESSED;
					Serial.print("Cleared Floor ");
					Serial.print(elevator1CurrentFloor);
					Serial.println(" Down Button");
				}
				// Stop the elevator
				elevator1Direction = DIR_NONE;
			}
			else										// Keep Moving
			{
				if(elevator1Direction == DIR_UP)			elevator1CurrentFloor ++;
				else										elevator1CurrentFloor --;
				Serial.print("Moving to floor ");
				Serial.println(elevator1CurrentFloor);
			}		
		}
	}
}
*/

elevatorClass elevator;  // Pre-instantiate object
